﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace ParkingData
{
    public class ParkingSpot
    {
        public string Name { get; set; }
        public OpenStatus IsOpen { get; set; }
        public bool IsPaymentActive { get; set; }
        public bool StatusParkPlace { get; set; }
        public Coordinate Coordinate { get; set; }
        public int MaxCount { get; set; }
        public int FreeCount { get; set; }
        public string Address { get; set; }

        public ParkingSpot(string name, OpenStatus isopen, bool ispaymentactive, bool statusparkplace, Coordinate coordinate, int maxcount, int freecount)
        {
            Name = name;
            IsOpen = isopen;
            IsPaymentActive = ispaymentactive;
            StatusParkPlace = statusparkplace;
            Coordinate = coordinate;
            MaxCount = maxcount;
            FreeCount = freecount;
            Address = GoogleMapsWrapper.CoordinateToAddress(Coordinate);
        }
    }

    public enum OpenStatus
    {
        Disabled = -1, Closed = 0, Open = 1,
    }
}
