﻿using PService.Parking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PService.Maps
{
    public interface IMaps
    {
        string CoordinateToAddress(Coordinate coords);
        Coordinate AddressToCoordinate(string region);
        byte[] GetStaticImage(string url);
        string[] GetStaticMapImage(List<ParkingSpot> spots, string searchtext, string center, int zoom, int spotstoshow = 0);
    }
}
