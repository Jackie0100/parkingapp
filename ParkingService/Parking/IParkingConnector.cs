﻿using PService.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PService.Parking
{
    interface IParkingConnector
    {
        List<ParkingSpot> ParkingSpots { get; set; }
        List<ParkingSpot> RequestData(IMaps maps);
    }
}
