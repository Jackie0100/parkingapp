﻿using PService.Maps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PService.Parking
{
    class ParkingConnector : IParkingConnector
    {
        private DateTime LastRequest { get; set; }
        public List<ParkingSpot> ParkingSpots { get; set; }

        public ParkingConnector()
        {
            if (File.Exists("ParkingServiceData.xml") && (DateTime.Now - File.GetLastWriteTime("ParkingServiceData.xml")).Minutes < 1)
            {
                LoadData(XDocument.Load("ParkingServiceData.xml"), new GoogleMapsWrapper());
                return;
            }
            else
            {
                RequestData(new GoogleMapsWrapper());
            }
        }

        public List<ParkingSpot> RequestData(IMaps maps)
        {
            if ((DateTime.Now - LastRequest).Minutes < 1)
            {
                return ParkingSpots;
            }

            WebRequest wreq = WebRequest.Create("http://ucn-parking.herokuapp.com/places.xml");
            WebResponse wres = wreq.GetResponse();
            Stream responseStream = wres.GetResponseStream();

            XDocument doc = XDocument.Load(responseStream);
            LastRequest = DateTime.Now;
            doc.Save("ParkingServiceData.xml");

            LoadData(doc, maps);

            //int index = -1;


            /*leteraltext += "['" + ParkingSpots[index].Name + "', " + ParkingSpots[index].Coordinate.Latitude.ToString(GoogleMapsWrapper.parserCultInfo) + ", " 
                + ParkingSpots[index].Coordinate.Longitude.ToString(GoogleMapsWrapper.parserCultInfo) + ", "
                + "'" + ParkingSpots[index].Name + "<br /> Availble Parking spots: " + ParkingSpots[index].FreeCount + "<br /> Paid Parking: "
                + (ParkingSpots[index].IsPaymentActive ? "Yes" : "No") + "<br /> Address: " + ParkingSpots[index].Address + "'" + "], ";*/
            return ParkingSpots;
        }


        protected void LoadData(XDocument xdoc, IMaps maps)
        {
            ParkingSpots = new List<ParkingSpot>();
            foreach (var place in xdoc.Descendants("object"))
            {
                ParkingSpots.Add(new ParkingSpot(
                    name: place.Element("name").Value.ToString(),
                    isopen: (OpenStatus)int.Parse(place.Element("is-open").Value.ToString()),
                    ispaymentactive: Convert.ToBoolean(int.Parse(place.Element("is-payment-active").Value.ToString())),
                    statusparkplace: Convert.ToBoolean(int.Parse(place.Element("status-park-place").Value.ToString())),
                    coordinate: new Coordinate(double.Parse(place.Element("latitude").Value.ToString(), PService.ParkingService.parserCultInfo),
                                            double.Parse(place.Element("longitude").Value.ToString(), PService.ParkingService.parserCultInfo)),
                    maxcount: int.Parse(place.Element("max-count").Value.ToString()),
                    freecount: int.Parse(place.Element("free-count").Value.ToString()),
                    maps: maps
                ));
            }
        }
    }
}
