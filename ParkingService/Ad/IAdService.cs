﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PService.AdServiceReference;

namespace PService.Ad
{
    interface IAdService
    {
        AdServiceReference.Ad GetAd();
        Image ConvertByteArrToImage(byte[] imgAsBytes);
    }
}
