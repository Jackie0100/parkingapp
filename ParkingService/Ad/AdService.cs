﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace PService.Ad
{
    class AdService : IAdService
    {
        public AdServiceReference.Ad GetAd()
        {
            //TODO: Exception handling

            System.ServiceModel.Channels.Binding binding = new System.ServiceModel.BasicHttpBinding();
            
            //TODO: remove debugging timeouts
            binding.OpenTimeout = new TimeSpan(1, 0, 0);
            binding.ReceiveTimeout = new TimeSpan(1, 0, 0);
            binding.CloseTimeout = new TimeSpan(1, 0, 0);
            binding.SendTimeout = new TimeSpan(1, 0, 0);

            AdServiceReference.IAdService client = new AdServiceReference.AdServiceClient(new System.ServiceModel.BasicHttpBinding(), new System.ServiceModel.EndpointAddress(@"http://localhost:9966/AdServiceLib/AdService/"));
            return client.GetRandomAd();
        }


        public Image ConvertByteArrToImage(byte[] imgAsBytes)
        {
            MemoryStream ms = new MemoryStream(imgAsBytes);
            Image result = Image.FromStream(ms);
            ms.Close();
            return result;
        }
    }
}
