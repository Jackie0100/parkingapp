﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;
using PService.Maps;
using PService.Mail;
using PService.Parking;
using System.Globalization;

namespace PService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ParkingService : IParkingService
    {
        public static CultureInfo parserCultInfo = CultureInfo.CreateSpecificCulture("en-US");
        IMaps mapsconnector = new GoogleMapsWrapper();
        IMail mailConnector = new Mail.ParkingServiceMail();
        IParkingConnector parkingConnector = new ParkingConnector();


        public bool SendMail(string email, Coordinate coords, Coordinate center, int zoomlevel)
        {
            mailConnector.SendMail(email, parkingConnector.RequestData(mapsconnector), coords, center, zoomlevel, mapsconnector);
            //ParkingService.Main(null);
            return true;
        }

        public List<ParkingSpot> GetParkingSpots()
        {
            return parkingConnector.RequestData(mapsconnector);
        }

        /* Use a test client
        public static void Main(string[] args)
        {
            Console.ReadLine();
        }
         */
    }
}
