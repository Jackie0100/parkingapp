﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using PService.Maps;
using PService.Parking;

namespace PService.Mail
{
    interface IMail
    {
        SmtpClient Client { get; set; }
        //MailMessage Message { get; set; }
        void SendMail(string recieveremail, List<ParkingSpot> spots, Coordinate coords, Coordinate center, int zoomlevel, IMaps maps);
    }
}
