﻿using PService.Parking;
using PService.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace PService.Mail
{
    public sealed class ParkingServiceMail : IMail
    {
        public SmtpClient Client { get; set; }

        //public MailMessage Message { get; set; }

        private Queue<MailMessage> mmq;
        private Thread mailSendingThread;
        private bool runSendThread;

        private readonly int SEND_THREAD_SLEEP_TIME = 500; //ms
        private readonly int SEND_THREAD_MAX_IDLE = 10000; //ms

        public ParkingServiceMail()
        {
            SetupClient();
            mmq = new Queue<MailMessage>();
            SetupMailSenderThread();
            
        }

        private const string EMAIL = "noreply.parkingservice@gmail.com";
        private const string PASSWORD = "romkugler";
        private const string SMTPADDRESS = "smtp.gmail.com";
        private const int SMTPPORT = 587;

        private const string AD_LOGO_CID = "adImage";
        private const string MAP_CID = "mapImage";

        private void SetupClient()
        {
            Client = new SmtpClient(SMTPADDRESS, SMTPPORT);
            Client.UseDefaultCredentials = true;
            Client.EnableSsl = true;
            Client.DeliveryMethod = SmtpDeliveryMethod.Network;
            Client.Timeout = 600000; //the ad images take some time to send.
            Client.Credentials = new System.Net.NetworkCredential(EMAIL, PASSWORD);
        }

        private void SetupMailSenderThread()
        {
            runSendThread = true;
            mailSendingThread = new Thread(() =>
            {

                Stopwatch idleTimer = new Stopwatch();
                idleTimer.Start();
                //TODO: make some way to terminate this
                while (runSendThread)
                {
                    if (mmq.Count > 0)
                    {
                        MailMessage msg = mmq.Dequeue();

                        try
                        {
                            Client.Send(msg);

                        }
                        catch (InvalidOperationException ex)
                        {
                            //this happens if Client.Send is still working on sending the previous mail. Amongst other things...
                            //TODO: check for other reasons InvalidOperationException would be thrown, and handle them accordingly
                        }

                        idleTimer.Restart();
                    }
                    else
                    {
                        if (idleTimer.ElapsedMilliseconds > SEND_THREAD_MAX_IDLE)
                        {
                            runSendThread = false;
                        }
                    }

                    //if idle, end imidiatly
                    if (runSendThread)
                    {
                        Thread.Sleep(SEND_THREAD_SLEEP_TIME);
                    }
                }
            });
        }

        public void SendMail(string recieveremail, List<ParkingSpot> spots, Coordinate coords, Coordinate center, int zoomlevel, IMaps maps)
        {
            /* Moved to SetupClient (called from constructor)
            Client = new SmtpClient(SMTPADDRESS, SMTPPORT);
            Client.UseDefaultCredentials = true;
            Client.EnableSsl = true;
            Client.DeliveryMethod = SmtpDeliveryMethod.Network;
            Client.Timeout = 600000; //the ad images take some time to send.
            Client.Credentials = new System.Net.NetworkCredential(EMAIL, PASSWORD);
            */
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(recieveremail));
            message.From = new MailAddress(EMAIL, "Parking App");
            message.Subject = "Automated Message Request from Parking App";
            string[] mailobj = maps.GetStaticMapImage(spots, coords.ToString(PService.ParkingService.parserCultInfo), center.ToString(PService.ParkingService.parserCultInfo), zoomlevel);

            message.IsBodyHtml = true;

            //Add an ad

            //TODO: move this somewhere else?
            Ad.IAdService adService = new Ad.AdService();
            AdServiceReference.Ad ad = adService.GetAd();

            string body =  mailobj[0] + " <br /> <img src=\"cid:"+ MAP_CID +"\"/><br /> <br /> <a href=\""+ ad.Link + "\"><img src=\"cid:" + AD_LOGO_CID + "\"/></a>";

            AlternateView aw = AlternateView.CreateAlternateViewFromString(body, new System.Net.Mime.ContentType("text/html"));
            aw.LinkedResources.Add(ConvertImage(maps.GetStaticImage(mailobj[1]), MAP_CID));

            aw.LinkedResources.Add(ConvertImage(ad.Logo, AD_LOGO_CID));

            message.AlternateViews.Add(aw);
            
            //send msg
            mmq.Enqueue(message);

            if (mailSendingThread.ThreadState == System.Threading.ThreadState.Stopped)
            {
                SetupMailSenderThread();
            }

            //not else if, as both ifs can be true in turn
            if (mailSendingThread.ThreadState == System.Threading.ThreadState.Unstarted)
            {
                mailSendingThread.Start();
            }

            /* moved to seperate thread
            try
            {
                while (mmq.Count > 0)
                {
                    MailMessage msg = mmq.Dequeue();
                    Client.Send(msg);
                }
            }
            catch (InvalidOperationException ex)
            {
                //this happens if Client.Send is still working on sending the previous mail. Amongst other things...
                //this doens't matter, as the message has already been queued :)

                //TODO: check for other reasons InvalidOperationException would be thrown, and handle them accordingly
            }
            */
        }

        /// <summary>
        /// Converts an image (in byte[] form from MemoryStream) into a LinkedResource.
        /// </summary>
        /// <param name="imgFromMemStream"></param>
        /// <returns></returns>
        private LinkedResource ConvertImage(byte[] imgFromMemStream, string ImageCID)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(imgFromMemStream);
            LinkedResource lres = new LinkedResource(ms, "image/png");
            lres.ContentId = ImageCID;

            return lres;
        }
    }
}
