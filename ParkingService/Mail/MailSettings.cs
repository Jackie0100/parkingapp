﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using PService.Maps;

namespace PService.Mail
{
    public abstract class MailSettings : IMail
    {
        public SmtpClient Client { get; set; }

        public MailMessage Message { get; set; }

        protected internal const string EMAIL = "";
        protected internal const string PASSWORD = "";
        protected internal const string SMTPADDRESS = "";
        protected internal const int SMTPPORT = 587;



        public abstract void SendMail(Coordinate coords, string email);
    }
}
