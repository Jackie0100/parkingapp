﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Website.ParkingServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Coordinate", Namespace="http://schemas.datacontract.org/2004/07/PService.Maps")]
    [System.SerializableAttribute()]
    public partial class Coordinate : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private double Latitudek__BackingFieldField;
        
        private double Longitudek__BackingFieldField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Latitude>k__BackingField", IsRequired=true)]
        public double Latitudek__BackingField {
            get {
                return this.Latitudek__BackingFieldField;
            }
            set {
                if ((this.Latitudek__BackingFieldField.Equals(value) != true)) {
                    this.Latitudek__BackingFieldField = value;
                    this.RaisePropertyChanged("Latitudek__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Longitude>k__BackingField", IsRequired=true)]
        public double Longitudek__BackingField {
            get {
                return this.Longitudek__BackingFieldField;
            }
            set {
                if ((this.Longitudek__BackingFieldField.Equals(value) != true)) {
                    this.Longitudek__BackingFieldField = value;
                    this.RaisePropertyChanged("Longitudek__BackingField");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ParkingSpot", Namespace="http://schemas.datacontract.org/2004/07/PService.Parking")]
    [System.SerializableAttribute()]
    public partial class ParkingSpot : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string Addressk__BackingFieldField;
        
        private Website.ParkingServiceReference.Coordinate Coordinatek__BackingFieldField;
        
        private int FreeCountk__BackingFieldField;
        
        private Website.ParkingServiceReference.OpenStatus IsOpenk__BackingFieldField;
        
        private bool IsPaymentActivek__BackingFieldField;
        
        private int MaxCountk__BackingFieldField;
        
        private string Namek__BackingFieldField;
        
        private bool StatusParkPlacek__BackingFieldField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Address>k__BackingField", IsRequired=true)]
        public string Addressk__BackingField {
            get {
                return this.Addressk__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.Addressk__BackingFieldField, value) != true)) {
                    this.Addressk__BackingFieldField = value;
                    this.RaisePropertyChanged("Addressk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Coordinate>k__BackingField", IsRequired=true)]
        public Website.ParkingServiceReference.Coordinate Coordinatek__BackingField {
            get {
                return this.Coordinatek__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.Coordinatek__BackingFieldField, value) != true)) {
                    this.Coordinatek__BackingFieldField = value;
                    this.RaisePropertyChanged("Coordinatek__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<FreeCount>k__BackingField", IsRequired=true)]
        public int FreeCountk__BackingField {
            get {
                return this.FreeCountk__BackingFieldField;
            }
            set {
                if ((this.FreeCountk__BackingFieldField.Equals(value) != true)) {
                    this.FreeCountk__BackingFieldField = value;
                    this.RaisePropertyChanged("FreeCountk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<IsOpen>k__BackingField", IsRequired=true)]
        public Website.ParkingServiceReference.OpenStatus IsOpenk__BackingField {
            get {
                return this.IsOpenk__BackingFieldField;
            }
            set {
                if ((this.IsOpenk__BackingFieldField.Equals(value) != true)) {
                    this.IsOpenk__BackingFieldField = value;
                    this.RaisePropertyChanged("IsOpenk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<IsPaymentActive>k__BackingField", IsRequired=true)]
        public bool IsPaymentActivek__BackingField {
            get {
                return this.IsPaymentActivek__BackingFieldField;
            }
            set {
                if ((this.IsPaymentActivek__BackingFieldField.Equals(value) != true)) {
                    this.IsPaymentActivek__BackingFieldField = value;
                    this.RaisePropertyChanged("IsPaymentActivek__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<MaxCount>k__BackingField", IsRequired=true)]
        public int MaxCountk__BackingField {
            get {
                return this.MaxCountk__BackingFieldField;
            }
            set {
                if ((this.MaxCountk__BackingFieldField.Equals(value) != true)) {
                    this.MaxCountk__BackingFieldField = value;
                    this.RaisePropertyChanged("MaxCountk__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<Name>k__BackingField", IsRequired=true)]
        public string Namek__BackingField {
            get {
                return this.Namek__BackingFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.Namek__BackingFieldField, value) != true)) {
                    this.Namek__BackingFieldField = value;
                    this.RaisePropertyChanged("Namek__BackingField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Name="<StatusParkPlace>k__BackingField", IsRequired=true)]
        public bool StatusParkPlacek__BackingField {
            get {
                return this.StatusParkPlacek__BackingFieldField;
            }
            set {
                if ((this.StatusParkPlacek__BackingFieldField.Equals(value) != true)) {
                    this.StatusParkPlacek__BackingFieldField = value;
                    this.RaisePropertyChanged("StatusParkPlacek__BackingField");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OpenStatus", Namespace="http://schemas.datacontract.org/2004/07/PService.Parking")]
    public enum OpenStatus : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Disabled = -1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Closed = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Open = 1,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ParkingServiceReference.IParkingService")]
    public interface IParkingService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/SendMail", ReplyAction="http://tempuri.org/IParkingService/SendMailResponse")]
        bool SendMail(string email, Website.ParkingServiceReference.Coordinate coords, Website.ParkingServiceReference.Coordinate center, int zoomlevel);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/GetParkingSpots", ReplyAction="http://tempuri.org/IParkingService/GetParkingSpotsResponse")]
        Website.ParkingServiceReference.ParkingSpot[] GetParkingSpots();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IParkingServiceChannel : Website.ParkingServiceReference.IParkingService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ParkingServiceClient : System.ServiceModel.ClientBase<Website.ParkingServiceReference.IParkingService>, Website.ParkingServiceReference.IParkingService {
        
        public ParkingServiceClient() {
        }
        
        public ParkingServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ParkingServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ParkingServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ParkingServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool SendMail(string email, Website.ParkingServiceReference.Coordinate coords, Website.ParkingServiceReference.Coordinate center, int zoomlevel) {
            return base.Channel.SendMail(email, coords, center, zoomlevel);
        }
        
        public Website.ParkingServiceReference.ParkingSpot[] GetParkingSpots() {
            return base.Channel.GetParkingSpots();
        }
    }
}
