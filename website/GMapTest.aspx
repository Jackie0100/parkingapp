﻿<%@ Page Title="Google Maps API Test" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="GMapTest.aspx.cs" Inherits="Jacksendary.GMapTest" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true">
    </script>

    <asp:Literal ID="Literal" runat="server"></asp:Literal>
    <h2>
        Google Map Demo:</h2>
    <div id="GoogleMapContainer">
    </div>
</asp:Content>
