﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ParkingServiceTest
{
    class Program
    {

        static void Main(string[] args)
        {
            WebRequest wreq = WebRequest.Create("http://ucn-parking.herokuapp.com/places.xml");
            WebResponse wres = wreq.GetResponse();
            Stream responseStream = wres.GetResponseStream();

            XDocument doc = XDocument.Load(responseStream);

            ParkingLocations parkingLocations = new ParkingLocations();

            foreach(var place in doc.Descendants("object"))
            {
                ParkingSpot parkingSpot = new ParkingSpot();

                parkingSpot.Name = place.Element("name").Value.ToString();
                parkingSpot.IsOpen = Int32.Parse(place.Element("is-open").Value.ToString());
                parkingSpot.IsPaymentActive = Int32.Parse(place.Element("is-payment-active").Value.ToString());
                parkingSpot.StatusParkPlace = Int32.Parse(place.Element("status-park-place").Value.ToString());
                parkingSpot.Longitude = Double.Parse(place.Element("longitude").Value.ToString());
                parkingSpot.Latitude = Double.Parse(place.Element("latitude").Value.ToString());
                parkingSpot.MaxCount = Int32.Parse(place.Element("max-count").Value.ToString());
                parkingSpot.FreeCount = Int32.Parse(place.Element("free-count").Value.ToString());

                parkingLocations.ParkingSpotsList.Add(parkingSpot);
            }

            /**
            Name: Navn på parkeringsplads
            IsOpen: -1: Disabled 0: Lukket 1: Åben
            IsPaymentActive: 1: Betalingsperiode er aktiv
            Status: 0: Error 1: OK
            Long / Lat: GPS Position
            MaxCount: Antal mulige pladser for parkeringsområde
            FreeCount Antal ledige pladser
            **/

            foreach (ParkingSpot ps in parkingLocations.ParkingSpotsList)
            {
                Console.WriteLine("Name: " + ps.Name);
                Console.WriteLine("Is it open: " + ps.IsOpen);
                Console.WriteLine("Is there payment: " + ps.IsPaymentActive);
                Console.WriteLine("Status: " + ps.StatusParkPlace);
                Console.WriteLine("Longtitude: " + ps.Longitude);
                Console.WriteLine("Latitude: " + ps.Latitude);
                Console.WriteLine("Maximum amount of parking spots: " + ps.MaxCount);
                Console.WriteLine("Available parking spots: " + ps.FreeCount);
                Console.WriteLine("********************************************");
            }

            Console.ReadLine();
        }
        
    }
}
