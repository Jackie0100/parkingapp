﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Web.Services;
using System.Net.Mail;
using Website.ParkingServiceReference;
using System.Globalization;

namespace website
{
    public partial class ParkingApp : System.Web.UI.Page
    {
        public static CultureInfo parserCultInfo = CultureInfo.CreateSpecificCulture("en-US");
        bool captchaValidated = false;
        //List<ParkingSpot> ParkingSpots = new List<ParkingSpot>();

        protected String SearchTextBoxProp
        {
            get
            {
                return SearchTextBox.Text;
            }
            set
            {
                SearchTextBox.Text = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Website.ParkingServiceReference.IParkingService service = new Website.ParkingServiceReference.ParkingServiceClient();

            string leteraltext = @"
                <script type='text/javascript'>
                    var map;
                    var markers;
                    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    function SearchLocation()
                    {
                        new google.maps.Geocoder().geocode({ 'address': document.getElementById('SearchTextBox').value }, function (results, status)
                        {
                            if (status == google.maps.GeocoderStatus.OK)
                            {
                                markers[0].setPosition(results[0].geometry.location);
                            }
                            else
                            {
                                alert('Geocode was not successful for the following reason: ' + status);
                            }
                            SetCenter();
                        });
                    }    
                    
                    function FindMyLocation()
                    {
                        if(!!navigator.geolocation)
                        {
                            navigator.geolocation.getCurrentPosition(function(position)
                            {
                                var geolocate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                markers[0].setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                                document.getElementById('SearchTextBox').value = position.coords.latitude + ',' + position.coords.longitude;
                                SetCenter();
                            });
                        }
                        else
                        {
                            alert('No Geolocation Support.');
                        }
                    }

                    function SetCenter()
                    {
                        // Centers the map to the markers
                        //  Create a new viewpoint bound
                        var bounds = new google.maps.LatLngBounds();
                        //  Go through each...
                        for (var i = 0; i < markers.length; i++)
                        {  
				            bounds.extend(markers[i].position);
                        }
                        //  Fit these bounds to the map
                        map.fitBounds(bounds);
                    }

                    function InitializeMap()
                    {       

                        var myOptions = {
                            zoom: 13,
                            center: new google.maps.LatLng(57.0477921, 9.9279943),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById('GoogleMapContainer'), myOptions);
                        markers = new Array();
                        markers.push(new google.maps.Marker({
                            map: map,
                            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                        }));
            ";
            WebRequest wreq = WebRequest.Create("http://ucn-parking.herokuapp.com/places.xml");
            WebResponse wres = wreq.GetResponse();
            Stream responseStream = wres.GetResponseStream();

            XDocument doc = XDocument.Load(responseStream);

            leteraltext += "var locations = [";

            ParkingSpot[] spots = service.GetParkingSpots();

            foreach (ParkingSpot spot in spots)
            {
                leteraltext += "['" + spot.Namek__BackingField + "', " + spot.Coordinatek__BackingField.Latitudek__BackingField.ToString(parserCultInfo) + ", "
                    + spot.Coordinatek__BackingField.Longitudek__BackingField.ToString(parserCultInfo) + ", " 
                    + "'" + spot.Namek__BackingField + "<br /> Availble Parking spots: " + spot.FreeCountk__BackingField + "<br /> Paid Parking: "
                    + (spot.IsPaymentActivek__BackingField ? "Yes" : "No") + "<br /> Address: " + spot.Addressk__BackingField + "'" + "], ";
            }
            leteraltext += "];\n";

            leteraltext += @"
                var infowindow = new google.maps.InfoWindow({
                    maxWidth: 800
                });                

                for (var i = 0; i < locations.length; i++)
                {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        title: locations[i][0]
                    });
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'click', (function(marker, i) 
                    {
                        return function()
                        {
                            infowindow.setContent(locations[i][3]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    map.addListener('idle', function() {
                        document.getElementById('HiddenField').value = markers[0].getPosition().lat() + ',' + markers[0].getPosition().lng() +
                        ',' + map.getCenter().lat() + ',' + map.getCenter().lng() + ',' + map.getZoom().toString();
                    });
                }
                SetCenter();
            ";

            leteraltext += @"
                }
                window.onload = InitializeMap;
                
                </script>
            ";
            ScriptLiteral.Text = leteraltext;
        }

        protected void EmailButton_Click(object sender, EventArgs e)
        {
            try
            { 
                Website.ParkingServiceReference.IParkingService service = new Website.ParkingServiceReference.ParkingServiceClient();
                new MailAddress(EmailTextBox.Text);
                string[] data = HiddenField.Value.Split(',');

                Coordinate coordcenter = new Coordinate();
                Coordinate coordlocation = new Coordinate();

                coordlocation.Latitudek__BackingField = double.Parse(data[0], parserCultInfo);
                coordlocation.Longitudek__BackingField = double.Parse(data[1], parserCultInfo);

                coordcenter.Latitudek__BackingField = double.Parse(data[2], parserCultInfo);
                coordcenter.Longitudek__BackingField = double.Parse(data[3], parserCultInfo);

                service.SendMail(EmailTextBox.Text, coordlocation, coordcenter, int.Parse(data[4]));

                service = null;

            /*
            if (SearchTextBox.Text.Length < 3)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Enter and search first!');</script>", false);
                return;
            }

            try
            {
                if (!captchaValidated)
                {
                    captchaValidated = GoogleReCaptcha.Validate();
                }
                if (captchaValidated)
                {
                    EmailButton.Enabled = false;

                    SmtpClient smtp = new SmtpClient("smtp.unoeuro.com", 587);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Timeout = 20000;
                    smtp.Credentials = new System.Net.NetworkCredential("no-reply@jacksendary.dk", "x6tf2pedmnych4wk3r9gz5ab");

                    MailMessage mail = new MailMessage();
                    mail.To.Add(new MailAddress(EmailTextBox.Text));
                    mail.From = new MailAddress("no-reply@jacksendary.dk", "Parking App");
                    mail.Subject = "Automated Message Request from Parking App";
                    //string[] mailobj = GoogleMapsWrapper.GetStaticMapImage(ParkingSpots, SearchTextBox.Text, HiddenField.Value);
                    mail.Body = "<html><body><h1>Your map request from Parking App</h1><br /><br />" + "You have at: " + DateTime.Now + " from IP: " + Request.UserHostName + "(" +
                        Request.UserHostAddress + ") requested the following data from the parking app. Please note that all data is approximate and might not be 100% right.<br /><br />" +
                        "<img src=\"" + mailobj[1] + "\" alt=\"map\" > <br /><br />" + mailobj[0] + "</body></html>";
                    mail.IsBodyHtml = true;
                    smtp.Send(mail);

                    //TODO: Navigate to a success site!
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Hey, we got a retard that cant complete captchas!');</script>", false);
                } */
            }
            catch (FormatException ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('Your email is wrong fucktard');</script>", false);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", "<script>alert('You broke it asswipe!');</script>", false);
                EmailButton.Enabled = true;
            }
        }
    }
}