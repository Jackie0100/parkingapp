﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Web.Services;

namespace Jacksendary
{
    public partial class GMapTest : System.Web.UI.Page
    {
        //GOOGLE MAP API KEY:           AIzaSyBMaiKpQAHLXlbjYlELslSBDe6fw669YEE
        //GOOGLE STATIC MAP API KEY:    svNONEhZ-lynZxW3bG75RDiTk0I= 

        List<ParkingSpot> ParkingSpots = new List<ParkingSpot>();

        protected void Page_Load(object sender, EventArgs e)
        {
            string leteraltext = @"
                <script type='text/javascript'>
                    function InitializeMap() {       
                    var myOptions = {
                        zoom: 13,
                        center: new google.maps.LatLng(57.0477921, 9.9279943),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById('GoogleMapContainer'), myOptions);
                    var markers = new Array();
            ";
            WebRequest wreq = WebRequest.Create("http://ucn-parking.herokuapp.com/places.xml");
            WebResponse wres = wreq.GetResponse();
            Stream responseStream = wres.GetResponseStream();

            XDocument doc = XDocument.Load(responseStream);

            int index = -1;

            leteraltext += "var locations = [";

            foreach (var place in doc.Descendants("object"))
            {
                index++;
                ParkingSpots.Add(new ParkingSpot(
                    name:               place.Element("name").Value.ToString(),
                    isopen:             (OpenStatus)int.Parse(place.Element("is-open").Value.ToString()),
                    ispaymentactive:    Convert.ToBoolean(int.Parse(place.Element("is-payment-active").Value.ToString())),
                    statusparkplace:    Convert.ToBoolean(int.Parse(place.Element("status-park-place").Value.ToString())),
                    longitude:          double.Parse(place.Element("longitude").Value.ToString(), System.Globalization.CultureInfo.InvariantCulture),
                    latitude: double.Parse(place.Element("latitude").Value.ToString(), System.Globalization.CultureInfo.InvariantCulture),
                    maxcount:           int.Parse(place.Element("max-count").Value.ToString()),
                    freecount:          int.Parse(place.Element("free-count").Value.ToString())
                ));

                leteraltext += "['" + ParkingSpots[index].Name + "', " + ParkingSpots[index].Latitude.ToString(System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + ", " + ParkingSpots[index].Longitude.ToString(System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + ", " + "'" + ParkingSpots[index].Name + "<br /> Availble Parking spots: " + ParkingSpots[index].FreeCount + "<br /> Paid Parking: " + (ParkingSpots[index].IsPaymentActive ? "Yes" : "No") + "'" + "], ";
            }

            leteraltext += "];\n";

            leteraltext += @"
                var infowindow = new google.maps.InfoWindow({
                    maxWidth: 800
                });                

                for (var i = 0; i < locations.length; i++) {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        title: locations[i][0]
                    });
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i][3]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }


                // Centers the map to the markers
                //  Create a new viewpoint bound
                var bounds = new google.maps.LatLngBounds();
                //  Go through each...
                for (var i = 0; i < markers.length; i++) {  
				    bounds.extend(markers[i].position);
                }
                //  Fit these bounds to the map
                map.fitBounds(bounds);
            ";

            leteraltext += @"
                }
                window.onload = InitializeMap;
                
                </script>
            ";

            Literal.Text = leteraltext; 
        }



        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

        }

        [WebMethod]
        public static string Test(string name)
        {
            return "HEJ!";
        }
    }
}