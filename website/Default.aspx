﻿<%@ Page Title="Parking App" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="website.ParkingApp" %>

<%@ Register assembly="Jacksendary" namespace="GoogleReCaptcha" tagprefix="cc1" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <asp:Literal ID="ScriptLiteral" runat="server"></asp:Literal>
    <h2>Parking App Demo:</h2>
    
    <div id="GoogleMapContainer">
    </div>
    <br />
    <br />
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <script type="text/javascript" language="javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                if (args.get_error() != undefined) {
                    args.set_errorHandled(true);
                }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                Enter distination to find parkings nearby: 
                <asp:TextBox ID="SearchTextBox" runat="server" Width="256px" ClientIDMode="Static" />
                <asp:Button ID="FindLocationButton" runat="server" Text="Get my location" Width="125px" onclientclick="FindMyLocation(); return false;" CausesValidation="False" />
                <asp:Button ID="SearchButton" runat="server" Text="Search" Width="100px" onclientclick="SearchLocation(); return false;" />
                <br />
                <br />
                <br />
                Enter your email:
                <asp:TextBox ID="EmailTextBox" runat="server" Width="256" style="margin-left: 32px" />
                <asp:Button ID="EmailButton" runat="server" Text="Send Email" onclick="EmailButton_Click" Width="125px" CausesValidation="False" />
                <br />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <div id="GoogleReCaptchaContainer">
            <cc1:GoogleReCaptcha ID="GoogleReCaptcha" runat="server" PrivateKey="6LfrSRATAAAAAElFfLO2lzxh6m4VlZ22abtmEz0r" PublicKey="6LfrSRATAAAAAKz6DrfvZvvwe0imFfIRs99kqyAk" />
        </div>
        <asp:HiddenField ID="HiddenField" runat="server" ClientIDMode="Static" />
    </div>
</asp:Content>
