﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Drawing;

namespace AdServiceLib
{
    [ServiceContract]
    public interface IAdService
    {
        [OperationContract]
        Ad GetRandomAd();
    }

    [DataContract]
    public class Ad
    {
        public Ad(int id, string link, byte[] logo)
        {
            Id = id;
            Link = link;
            Logo = logo;
        }

        [DataMember]
        public string Link { get; set; }

        [DataMember]
        public byte[] Logo{ get; set; }

        [DataMember]
        public int Id { get; set; }
    }
}
