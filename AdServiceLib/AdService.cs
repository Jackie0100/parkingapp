﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Drawing;
using AdServiceLib.Properties;
using System.Resources;

namespace AdServiceLib
{
    public class AdService : IAdService
    {
        public Ad GetRandomAd()
        {
            return GenerateAd();
        }

        private Ad GenerateAd()
        {
            //string fpath = "webServer\\linkmaps.json";
            string jsonObj = "";

            jsonObj = Resources.linkmaps;

            /*StreamReader sr = new StreamReader(Environment.CurrentDirectory + fpath);
            while (!sr.EndOfStream)
            {
                jsonObj = sr.ReadLine();
            }
            sr.Close();
            */ 

            dynamic jsons = Util.JsonParser.Parse(jsonObj);
            

            List<Ad> ads = new List<Ad>();
            foreach (dynamic json in jsons.Children)
            { 


                //Image logo = Image.FromFile("webServer/img/" + json.img);

                string imgExcludeExt = ((string)json.img).Split('.')[0];

                Image logo = new Bitmap((Bitmap)Resources.ResourceManager.GetObject(imgExcludeExt, System.Globalization.CultureInfo.CurrentCulture));
                

                byte[] logoBytes = ConvertImageToBytes(logo);

                Ad ad = new Ad(Int32.Parse(json.id), json.link, logoBytes);
                ads.Add(ad);
            }

            Ad rndAd = ads[new Random().Next(ads.Count)];

            return rndAd;
           
        }

        /// <summary>
        /// Converts a PNG bitmap into a bytearray written by a memorystream.
        /// </summary>
        /// <param name="image">The image to convert</param>
        /// <returns></returns>
        private byte[] ConvertImageToBytes(Image image)
        {

            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] imageBytes = ms.ToArray();
            ms.Close();

            return imageBytes;
        }

        /// <summary>
        /// Converts a bytearray to an Image type by the means of a memorystream.
        /// </summary>
        /// <param name="imageAsBytes">the bytearray to convert to an Image type</param>
        /// <returns></returns>
        private Image ConvertBytesToImage(byte[] imageAsBytes)
        {
            MemoryStream ms = new MemoryStream(imageAsBytes);
            Image result = Image.FromStream(ms);
            ms.Close();
            return result;
        }


        public dynamic TestJSONParser()
        {
            StreamReader sr = new StreamReader(@"webServer\linkmaps.json");
            string jsonObj = "";
            while (!sr.EndOfStream)
            {
                jsonObj = sr.ReadLine();
            }
            sr.Close();

            return Util.JsonParser.Parse(jsonObj);
        }
    }
}
