﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdServiceLib.Util
{
    public class JsonParser
    {
        //TODO: make return type
        public static dynamic Parse(string str)
        {
            //TODO: analyze for validation
            char startChar = str[0];
            int endPos = str.Length - 1;
            char endChar = str[endPos];

            CodeBlock parent = HandleCodeBlocks(str, startChar, endChar);
            dynamic res = ConvertToDynamic(parent);
            return res;
        }

        private static CodeBlock HandleCodeBlocks(string str, char startChar, char endChar)
        {
            CodeBlock block = new CodeBlock(0, startChar, endChar);
            bool skip = false;  //<-- indicates if the rest of the for loop should be skipped, as we have already found the endChar
            int i;  //<-- for debugging purposes
            for (i = 1; i < str.Length && !skip; i++)
            {
                char c = str[i];

                switch (c)
                {
                    case '{':
                        CodeBlock newObj = HandleCodeBlocks(str.Substring(i, str.Length - 1 - i), c, '}');
                        newObj.StartPos = i + block.StartPos;
                        block.Push(newObj);
                        //skip what has already been written in the new block:
                        //i += newObj.Length;
                        i += newObj.Code.Length + 1;    //+1 = end-char
                        break;

                    case '}':
                        //this will always be true (unless the JSON has invalid format)
                        if (c == endChar)
                        {
                            //block.Length = i - block.StartPos;
                            skip = true;
                        }
                        else
                        {
                            throw new JsonException("wrong block ending char", str);
                        }
                        break;

                    case '[':
                        CodeBlock newArr = HandleCodeBlocks(str.Substring(i, str.Length - 2 - i), c, ']');
                        newArr.StartPos = i + block.StartPos;
                        block.Push(newArr);
                        //i += newArr.Length;
                        i += newArr.Code.Length + 1;     //+1 = end-char;
                        break;

                    case ']':
                        if (c == endChar)
                        {
                            //block.Length = i - block.StartPos;
                            skip = true;
                        }
                        break;
                }

                if (!skip)
                {
                    block.Code += c;
                }
            }

            //if false, then the block never closed!
            if (!skip)
            {
                throw new JsonException("Invalid Json: block was not closed!", str);
            }

            //return parentBlock;
            return block;
        }

        private static dynamic ConvertToDynamic(CodeBlock parent)
        {
            dynamic res = new ExpandoObject();

            List<dynamic> dynChildren = new List<dynamic>();
            int[] childPos = new int[parent.Count];
            
            CodeBlock child = parent.Pop();
            int childPosIndex = 0;
            while (child != null)
            {
                childPos[childPosIndex] = child.StartPos;
                childPosIndex++;

                dynamic dynChild = ConvertToDynamic(child);

                dynChildren.Add(dynChild);
                child = parent.Pop();
            }

            res.Children = dynChildren;


            string keyStr = "";
            string valStr = "";
            bool inline = false;
            bool key = true;

            for (int i = 0; i < parent.Code.Length; i++)
            {
                bool skipSwitch = false;
                foreach (int cp in childPos)
                {
                    if (i == cp)
                    {
                        skipSwitch = true;
                        break;
                    }
                }

                if (!skipSwitch)
                {
                    char c = parent.Code[i];
                    switch (c)
                    {
                        case '"':
                            inline = !inline;
                            break;

                        case ':':
                            if (inline)
                            {
                                if (key && inline) throw new JsonException("\":\" was used in key", parent.Code);
                                else valStr += c;
                            }
                            else
                            {
                                key = false;
                            }
                            break;

                        case ',':
                            if (inline)
                            {
                                if (key && inline) throw new JsonException("\",\" was used in key", parent.Code);
                                else valStr += c;
                            }
                            else
                            {
                                //only add to res if there is a key value
                                if (!keyStr.Equals(""))
                                {
                                    ((IDictionary<string, object>)res).Add(keyStr, valStr);
                                }

                                keyStr = "";
                                valStr = "";
                                key = true;
                                
                            }
                            break;

                        case '{':
                        case '[':
                        case ']':
                        case '}':
                        case ' ':
                            if (inline)
                                if (key) throw new JsonException(c + " was used in key", parent.Code);
                                else valStr += c;
                            //else ignore
                            break;

                        default:
                            if (key) keyStr += c;
                            else valStr += c;
                            break;
                    }
                }
            }

            //add the last element to the res
            if (!keyStr.Equals(""))
            {
                ((IDictionary<string, object>)res).Add(keyStr, valStr);
            }

            return res;
        }

        private class CodeBlock
        {
            private Stack<CodeBlock> children;

            public CodeBlock(int startPos, char start, char end/*, int length = -1*/)
            {
                StartPos = startPos;
                Start = start;
                End = end;
                //Length = length;

                Code = "";

                children = new Stack<CodeBlock>();
            }

            public int StartPos { get; set; }
            public char Start { get; private set; }
            public char End { get; private set; }
            //public int Length { get; set; }

            public string Code { get; set; }

            public int Count { get{ return children.Count; } }

            public void Push(CodeBlock child)
            {
                children.Push(child);
            }

            public CodeBlock Pop()
            {
                if (children.Count > 0)
                {
                    return children.Pop();
                }
                else
                {
                    return null;
                }
            }

            public string GetFamilyCode(string childPrefix = "")
            {
                Dictionary<int, CodeBlock> positoinLabeledChildren = new Dictionary<int,CodeBlock>();

                while(children.Count > 0)
                {
                    CodeBlock child = children.Pop();
                    positoinLabeledChildren.Add(child.StartPos + StartPos, child);
                }


                string res = "";
                for (int i = 0; i < Code.Length; i++)
                {
                    if (positoinLabeledChildren.ContainsKey(i))
                    {
                        CodeBlock child = positoinLabeledChildren[i];
                        res += childPrefix;
                        res += child.GetFamilyCode();
                    }
                    else
                    {
                        res += Code[i];
                    }
                }

                return res;
            }
        }
    }

    public class JsonException : Exception
    {
        public JsonException(string message, string json)
            : base(message)
        {
            this.Json = json;
        }

        public string Json { get; private set; }
    }
}
