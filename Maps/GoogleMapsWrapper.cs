﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Maps
{
    public static class GoogleMapsWrapper
    {
        #region Secret!
        private const string GOOGLESTATICMAPAPIKEY = "AIzaSyBMaiKpQAHLXlbjYlELslSBDe6fw669YEE";
        private const string GOOGLEMAPAPIKEY = "svNONEhZ-lynZxW3bG75RDiTk0I=";
        #endregion
        public static CultureInfo parserCultInfo = CultureInfo.CreateSpecificCulture("en-US");

        public static string CoordinateToAddress(Coordinate coords)
        {
            string requestUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng=" + coords.Latitude.ToString(parserCultInfo) + "," + coords.Longitude.ToString(parserCultInfo) + "&sensor=false";

            using (WebClient wc = new WebClient())
            {
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where
                                  elm.Name == "status"
                              select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    var res = (from elm in xmlElm.Descendants()
                               where
                                   elm.Name == "formatted_address"
                               select elm).FirstOrDefault();
                    requestUri = res.Value;
                }
            }
            return requestUri;
        }

        public static Coordinate AddressToCoordinate(string region)
        {
            string requestUri = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + region;

            using (WebClient wc = new WebClient())
            {
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where
                                  elm.Name == "status"
                              select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    return new Coordinate(
                        double.Parse((from elm in xmlElm.Descendants() where elm.Name == "lat" select elm).FirstOrDefault().Value, parserCultInfo),
                        double.Parse((from elm in xmlElm.Descendants() where elm.Name == "lng" select elm).FirstOrDefault().Value, parserCultInfo));
                }
                else
                {
                    throw new Exception("The service is currently unavailble!");
                }
            }
        }

        public static string[] GetStaticMapImage(List<ParkingSpot> spots, string searchtext, string centerzoom, int spotstoshow = 0)
        {
            string[] mapobj = new string[2];
            mapobj[1] = "https://maps.googleapis.com/maps/api/staticmap?" + centerzoom + "&size=512x512&scale=2";

            Coordinate coords = AddressToCoordinate(searchtext);
            mapobj[1] += "&markers=color:green%7Clabel:M%7C" + coords.ToString();
            mapobj[0] += "<h2>Label Green M: the location you searched on the homepage!</h2><br /><br />";

            List<ParkingSpot> parkingspots = spots.OrderBy(p => p.Coordinate.Disstance(coords)).ToList<ParkingSpot>();

            if (spotstoshow <= 0)
            {
                if (parkingspots.Count > 26)
                {
                    spotstoshow = 26;
                }
                else
                {
                    spotstoshow = parkingspots.Count;
                }
            }

            for (int i = 0; i < spotstoshow; i++)
            {
                if (parkingspots[i].IsOpen != OpenStatus.Open || parkingspots[i].FreeCount == 0)
                {
                    parkingspots.RemoveAt(i);
                    i--;
                    if (spotstoshow > parkingspots.Count)
                    {
                        spotstoshow = parkingspots.Count;
                    }
                    continue;
                }
                mapobj[0] += "<h2>Label Red " + ((char)(i + 65)).ToString() + ": " + parkingspots[i].Name + "</h2><br /> Availble Parking spots: " +
                    parkingspots[i].FreeCount + "/" + parkingspots[i].MaxCount + "<br /> Paid Parking: " + (parkingspots[i].IsPaymentActive ? "Yes" : "No") +
                    "<br /> Address: " + parkingspots[i].Address + "<br /><br /><br />";
                mapobj[1] += "&markers=color:red%7Clabel:" + ((char)(i + 65)).ToString() + "%7C" + parkingspots[i].Coordinate.ToString();
            }
            mapobj[1] += "&key=" + GOOGLESTATICMAPAPIKEY;
            return mapobj;
        }
    }
}