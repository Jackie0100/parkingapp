﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Maps
{
    public class Coordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Coordinate(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override string ToString()
        {
            return Latitude + "," + Longitude;
        }

        public double Disstance(Coordinate coords)
        {
            double lat1 = Latitude * (Math.PI / 180);
            double lat2 = coords.Latitude * (Math.PI / 180);
            double long1 = Longitude * (Math.PI / 180);
            double long2 = coords.Longitude * (Math.PI / 180);

            double dlong = (long2 - long1);
            double dlat  = (lat2 - lat1);

            // Haversine formula:
            double R = 6371;
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Sin(dlong / 2) * Math.Sin(dlong / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d;
        }

        public static double ToRadians(double degrees)
        {
            return degrees * (Math.PI / 180);
        }
    }
}