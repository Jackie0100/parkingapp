﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestClient.ParkingServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ParkingServiceReference.IParkingService")]
    public interface IParkingService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/SendMail", ReplyAction="http://tempuri.org/IParkingService/SendMailResponse")]
        bool SendMail(string email, PService.Maps.Coordinate coords, PService.Maps.Coordinate center, int zoomlevel);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/SendMail", ReplyAction="http://tempuri.org/IParkingService/SendMailResponse")]
        System.Threading.Tasks.Task<bool> SendMailAsync(string email, PService.Maps.Coordinate coords, PService.Maps.Coordinate center, int zoomlevel);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/GetParkingSpots", ReplyAction="http://tempuri.org/IParkingService/GetParkingSpotsResponse")]
        PService.Parking.ParkingSpot[] GetParkingSpots();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IParkingService/GetParkingSpots", ReplyAction="http://tempuri.org/IParkingService/GetParkingSpotsResponse")]
        System.Threading.Tasks.Task<PService.Parking.ParkingSpot[]> GetParkingSpotsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IParkingServiceChannel : TestClient.ParkingServiceReference.IParkingService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ParkingServiceClient : System.ServiceModel.ClientBase<TestClient.ParkingServiceReference.IParkingService>, TestClient.ParkingServiceReference.IParkingService {
        
        public ParkingServiceClient() {
        }
        
        public ParkingServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ParkingServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ParkingServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ParkingServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool SendMail(string email, PService.Maps.Coordinate coords, PService.Maps.Coordinate center, int zoomlevel) {
            return base.Channel.SendMail(email, coords, center, zoomlevel);
        }
        
        public System.Threading.Tasks.Task<bool> SendMailAsync(string email, PService.Maps.Coordinate coords, PService.Maps.Coordinate center, int zoomlevel) {
            return base.Channel.SendMailAsync(email, coords, center, zoomlevel);
        }
        
        public PService.Parking.ParkingSpot[] GetParkingSpots() {
            return base.Channel.GetParkingSpots();
        }
        
        public System.Threading.Tasks.Task<PService.Parking.ParkingSpot[]> GetParkingSpotsAsync() {
            return base.Channel.GetParkingSpotsAsync();
        }
    }
}
