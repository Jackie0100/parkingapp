﻿using AdServiceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using PService;

namespace TestClient
{
    class Program
    {
        private static readonly string AD_PORT = "9966";
        private static readonly string PARKING_PORT = "9967";

        private static bool runAdService;
        private static bool runParkingService;

        static void Main(string[] args)
        {
            runAdService = true;
            runParkingService = true;

            Thread adThread = null;
            Thread parkingThread = null;

            Dictionary<string, Action> commands = new Dictionary<string, Action>();

            bool comLoop = true;

            commands.Add("quit", () => { comLoop = false; });
            commands.Add("exit", () => { comLoop = false; });
            commands.Add("start ad", () => {
                adThread = new Thread(() =>
                {
                    StartAdService();
                });
                adThread.Start(); 
            });
            commands.Add("start parking", () => {
                parkingThread  = new Thread(() =>
                {
                    StartParkingService();
                }); 
                parkingThread.Start(); 
            });
            commands.Add("stop parking", () => {runParkingService = false; });
            commands.Add("stop ad", () => { runAdService = false;});
            commands.Add("help", () => { Console.WriteLine("Available commands: \n -------------------"); foreach (string str in commands.Keys) { Console.WriteLine("# " + str); }; Console.WriteLine(); });
            commands.Add("test mail", () => { TestSendMail(); });


            while (comLoop)
            {
                Console.WriteLine("Awaiting Command: ");
                string com = Console.ReadLine().ToLower();

                if (commands.Keys.Contains(com))
                {
                    commands[com]();
                }
                else if (!com.Equals(""))
                {
                    Console.WriteLine("Unknown command! Type \"help\" for a list of commands...");
                }
            }

            //close the other threads
            if(parkingThread != null) parkingThread.Abort();
            if(adThread != null) adThread.Abort();
        }

        private static void StartParkingService()
        {
            using (ServiceHost parkingHost = new ServiceHost(typeof(PService.ParkingService), new Uri("http://localhost:" + PARKING_PORT + "/PService/ParkingService/")))
            {
                parkingHost.OpenTimeout = new TimeSpan(1, 0, 0);
                parkingHost.CloseTimeout = new TimeSpan(1, 0, 0);
                parkingHost.Open();
                Console.WriteLine("# Parking host state: " + parkingHost.State);

                while (runParkingService)
                {
                    Thread.Sleep(1000);
                }

                parkingHost.Close();
            }

            Console.WriteLine("# Parking service is closed.");
        }

        private static void TestSendMail()
        {
            ParkingServiceReference.IParkingService ps = new ParkingServiceReference.ParkingServiceClient();

            Console.WriteLine("Write mail to send to (empty line = noreply.parkingservice@gmail.com): ");
            string mail = Console.ReadLine();
            mail = mail.Equals("") ? "noreply.parkingservice@gmail.com" : mail;

            Console.WriteLine("number of tests: ");
            string noStr = Console.ReadLine();
            int no = Int32.Parse(noStr);
            Console.WriteLine("Starting test ({0}) with {1} mail(s) after ENTER...", mail, no);
            Console.ReadLine();

            long longestTime = 0;
            long totalTime = 0;
            Parallel.For(0, no, (index) =>
            {
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                ps.SendMail(mail, new PService.Maps.Coordinate(57.0209892, 9.8813855), new PService.Maps.Coordinate(57.0209892, 9.8813855), 17);
                long snapshot = sw.ElapsedMilliseconds; //<--- to postpone the probe-effect (the added time for Console.WriteLine is part of the next send)
                Console.WriteLine("# Mail number {0} of {1} send. Total time: {2} ms", index + 1, no, snapshot);
                totalTime += snapshot;
                longestTime = snapshot > longestTime ? snapshot : longestTime;
                sw.Stop();
            });

            Console.WriteLine("# Done! Elapsed time: {0} ms \t Average: {1} ms.", longestTime, (float)totalTime / no);
        }

        private static void StartAdService()
        {
            using (ServiceHost adHost = new ServiceHost(typeof(AdService), new Uri("http://localhost:" + AD_PORT + "/AdServiceLib/AdService/")))
            {
                adHost.Open();
                Console.WriteLine("# Ad host is: " + adHost.State);

                while (runAdService)
                {
                    Thread.Sleep(1000);
                }

                adHost.Close();
            }

            Console.WriteLine("# Ad service is closed.");
        }

        private static string GetCommand()
        {
            return Console.ReadLine();
        }

        private static void PrintJsonObject(dynamic jsonObj, int tabCount)
        {
            IDictionary<string, object> dyndic = (IDictionary<string, object>)jsonObj;
            string tabs = " |";
            for (int i = 0; i < tabCount; i++)
            {
                tabs += "  |";
            }

            foreach (string key in dyndic.Keys.Reverse())
            {
                if (key == "Children")
                {
                    Console.WriteLine(tabs + key + ": \t" + ((List<object>)dyndic[key]).Count);
                }
                else
                {
                    Console.WriteLine(tabs + key + ": \t" + dyndic[key]);
                }
            }

            for (int i = 0; i < jsonObj.Children.Count; i++ )
            {
                Console.WriteLine(tabs + "-------------------------------");
                dynamic child = jsonObj.Children[i];
                PrintJsonObject(child, tabCount);
                
            }
        }
    }
}
